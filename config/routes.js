var Index = require('../app/controllers/index')
var Business = require('../app/controllers/business')
var Test = require('../app/controllers/test')

module.exports = function(app) {

  // pre handle user
  app.use(function(req, res, next) {
    var _user = req.session.user
    app.locals.user = _user
    next()
  })

  // Index
  app.get('/', Business.list)

  // Business
  app.get('/business/list', Business.list)
  app.get('/business/:id', Business.detail)
  app.get('/admin/business/input/', Business.input)
  app.get('/admin/business/input/:id', Business.input)
  app.post('/admin/business/save', Business.save)
  app.get('/admin/business/list', Business.adminlist)
  app.delete('/admin/business/delete/:id', Business.delete)

  // test
  app.get('/test', Test.test)


}