var mongoose = require('mongoose')
var Business = mongoose.model('Business')
var _ = require('underscore')
var fs = require('fs')
var path = require('path')
var qiniu = require('../utils/qiniu')

// list page
exports.list = function(req, res) {
  Business.find({status:0})
    .sort('-meta.updateAt')
    .exec(function(err, businesses) {
      if (err) {
        console.log(err)
      }
      res.render('business_list', {
        title: '海门印象',
        businesses: businesses
      })
    })
}

// detail page
exports.detail = function(req, res) {
  var id = req.params.id
  Business.update({_id: id}, {$inc: {pv: 1}}, function(err) {
    if (err) {
      console.log(err)
    }
  })
  Business.findById(id, function(err, business) {
    res.render('detail', {
      title: '详情页',
      business: business
    })
  })
}

// admin input page
exports.input = function(req, res) {
  var uploadToken = qiniu.genUpToken()
  var id = req.params.id
  Business.findById(id, function(err, business) {
    if (!business) business = {}
    var posterArr = business.poster
    var initPosterUrlArr = []
    if (posterArr) {
      for (var i=0; i<posterArr.length; i++) {
        var poster = posterArr[i]
        var url = qiniu.genDnURL(poster)
        initPosterUrlArr.push(url)
      }
    }
    res.render('admin/business_input', {
      title: '后台输入页',
      business: business,
      uploadToken: uploadToken,
      initPosterUrlArr: initPosterUrlArr
    })
  })
}

// admin post business
exports.save = function(req, res) {
  var id = req.body.business._id
  var businessObj = req.body.business
  var newBusiness
  if (id) {
    Business.findById(id, function(err, business) {
      if (err) console.log(err)
      newBusiness = _.extend(business, businessObj)
      newBusiness.save(function(err, business) {
        if (err) console.log(err)
        res.redirect('/admin/business/list')
      })
    })
  }
  else {
    newBusiness = new Business(businessObj)
    newBusiness.save(function(err, business) {
      if (err) console.log(err)
      res.redirect('/admin/business/list')
    })
  }
}

// admin list page
exports.adminlist = function(req, res) {
  Business.find({status:0})
    .sort('-meta.updateAt')
    .exec(function(err, businesses) {
      if (err) {
        console.log(err)
      }
      res.render('admin/business_list', {
        title: '列表页',
        businesses: businesses
      })
    })
}

// admin delete
exports.delete = function(req, res) {
  var id = req.params.id
  if (id) {
    Business.findById(id, function(err, business) {
      if (err) console.log(err)
      business.status = -1
      business.save(function(err, business) {
        if (err) {
          console.log(err)
          res.json({success: 0})
        }
        else {
          res.json({success: 1})
        }
      })
    })
  }
}