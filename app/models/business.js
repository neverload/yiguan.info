var mongoose = require('mongoose')
var BusinessSchema = require('../schemas/Business')
var Business = mongoose.model('Business', BusinessSchema)

module.exports = Business