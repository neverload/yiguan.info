var mongoose = require('mongoose')
var Schema = mongoose.Schema

var BusinessSchema = new Schema({
  name: String,
  address: String,
  phone: String,
  opentime: String,
  closetime: String,
  poster: [String],
  star: Number,
  cost: Number,
  summary: String,
  status: {
    type: Number,
    default: 0
  },
  meta: {
    createAt: {
      type: Date,
      default: Date.now()
    },
    updateAt: {
      type: Date,
      default: Date.now()
    }
  }
})

BusinessSchema.pre('save', function(next) {
  if (this.isNew) {
    this.meta.createAt = this.meta.updateAt = Date.now()
  }
  else {
    this.meta.updateAt = Date.now()
  }
  next()
})

BusinessSchema.statics = {
  fetch: function(cb) {
    return this
      .find()
      .sort('meta.updateAt')
      .exec(cb)
  },
  findById: function(id, cb) {
    return this
      .findOne({_id: id})
      .exec(cb)
  }
}

module.exports = BusinessSchema
